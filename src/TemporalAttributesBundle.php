<?php

/**
 * @copyright: Copyright (c) 2018, Present Progressive GbR
 * @author: Benedict Zinke <bz@presentprogressive.de>
 * @license : MIT
 */

namespace PresProg\TemporalAttributesBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class TemporalAttributesBundle extends Bundle
{

}