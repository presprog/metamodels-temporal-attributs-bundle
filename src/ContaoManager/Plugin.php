<?php

/**
 * @copyright: Copyright (c) 2018, Present Progressive GbR
 * @author: Benedict Zinke <bz@presentprogressive.de>
 * @license : MIT
 */

namespace PresProg\TemporalAttributesBundle\ContaoManager;

use Contao\ManagerPlugin\Bundle\BundlePluginInterface;
use Contao\ManagerPlugin\Bundle\Config\BundleConfig;
use Contao\ManagerPlugin\Bundle\Parser\ParserInterface;
use Contao\ManagerPlugin\Routing\RoutingPluginInterface;
use MetaModels\CoreBundle\MetaModelsCoreBundle;
use MetaModels\FilterTagsBundle\MetaModelsFilterTagsBundle;
use PresProg\TemporalAttributesBundle\TemporalAttributesBundle;
use Symfony\Component\Config\Loader\LoaderResolverInterface;
use Symfony\Component\HttpKernel\KernelInterface;

class Plugin implements BundlePluginInterface
{
    public function getBundles(ParserInterface $parser)
    {
        return [
            BundleConfig::create(
                TemporalAttributesBundle::class
            )->setLoadAfter([
                MetaModelsCoreBundle::class,
                MetaModelsFilterTagsBundle::class
            ])
        ];
    }


}