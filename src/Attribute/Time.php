<?php

/**
 * @copyright: Copyright (c) 2018, Present Progressive GbR
 * @author: Benedict Zinke <bz@presentprogressive.de>
 * @license : MIT
 */

namespace PresProg\TemporalAttributesBundle\Attribute;

use Contao\Config;
use Contao\System;
use Doctrine\DBAL\Connection;
use MetaModels\Attribute\BaseSimple;
use MetaModels\Helper\TableManipulator;
use MetaModels\IMetaModel;
use MetaModels\Render\Template;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * This is the MetaModelAttribute class for handling Time fields.
 */
class Time extends BaseSimple
{

    private $defaultFormat = 'H:i:s';

    /**
     * @return string
     */
    public function getDefaultFormat(): string
    {
        return $this->defaultFormat;
    }

    /**
     * The event dispatcher.
     *
     * @var EventDispatcherInterface
     */
    private $dispatcher;

    /**
     * Instantiate an MetaModel attribute.
     *
     * Note that you should not use this directly but use the factory classes to instantiate attributes.
     *
     * @param IMetaModel                    $objMetaModel     The MetaModel instance this attribute belongs to.
     * @param array                         $arrData          The information array for the attribute.
     * @param Connection                    $connection       The database connection.
     * @param TableManipulator              $tableManipulator Table manipulator instance.
     * @param EventDispatcherInterface|null $dispatcher       The event dispatcher.
     */
    public function __construct(
        IMetaModel $objMetaModel,
        array $arrData = [],
        Connection $connection = null,
        TableManipulator $tableManipulator = null,
        EventDispatcherInterface $dispatcher = null
    ) {
        parent::__construct($objMetaModel, $arrData, $connection, $tableManipulator);

        if (null === $dispatcher) {
            // @codingStandardsIgnoreStart
            @\trigger_error(
                'Table event dispatcher is missing. It has to be passed in the constructor. Fallback will be dropped.',
                E_USER_DEPRECATED
            );
            // @codingStandardsIgnoreEnd

            $dispatcher = System::getContainer()->get('event_dispatcher');
        }
        $this->dispatcher = $dispatcher;
    }

    /**
     * {@inheritDoc}
     */
    public function getSQLDataType()
    {
        return 'time NULL';
    }

    /**
     * {@inheritDoc}
     */
    public function getAttributeSettingNames()
    {
        return \array_merge(
            parent::getAttributeSettingNames(),
            [
                'filterable',
                'searchable',
                'mandatory',
            ]
        );
    }

    /**
     * {@inheritDoc}    #
     */
    public function getFieldDefinition($arrOverrides = [])
    {
        $arrFieldDef                 = parent::getFieldDefinition($arrOverrides);
        $arrFieldDef['eval']['rgxp'] = 'time';
        $arrFieldDef['inputType'] = 'text';

        if (empty($arrFieldDef['eval']['readonly'])) {
            $arrFieldDef['eval']['tl_class']  .= ' wizard';
        }

        return $arrFieldDef;
    }

    /**
     * {@inheritDoc}
     */
    protected function prepareTemplate(Template $objTemplate, $arrRowData, $objSettings)
    {
        parent::prepareTemplate($objTemplate, $arrRowData, $objSettings);

        $time = \DateTime::createFromFormat($this->getDefaultFormat(), $arrRowData[$this->getColName()]);

        if ($time) {
            $objTemplate->parsedTime = $time->format($this->getTimeFormat());
        } else {
            $objTemplate->parsedTime = '';
        }
    }

    /**
     * Retrieve the selected type or fallback to 'date' if none selected.
     *
     * @return string
     */
    public function getTimeFormat()
    {
        $page = $this->getObjPage();
        if ($page && $page->timeFormat) {
            return $page->timeFormat;
        }

        return Config::get('timeFormat');
    }

    /**
     * {@inheritdoc}
     */
    public function widgetToValue($varValue, $itemId)
    {
        if (null === $varValue) {
            return null;
        }

        $time = \DateTime::createFromFormat($this->getTimeFormat(), $varValue);

        if (!$time) {
            return null;
        }

        return $time->format($this->getDefaultFormat());
    }

    public function valueToWidget($varValue)
    {
        $time = \DateTime::createFromFormat($this->getDefaultFormat(), $varValue);

        if (!$time) {
            return $varValue;
        }

        return $time->format($this->getTimeFormat());
    }

    /**
     * Returns the global page object (replacement for super globals access).
     *
     * @return mixed The global page object
     *
     * @SuppressWarnings(PHPMD.Superglobals)
     * @SuppressWarnings(PHPMD.CamelCaseVariableName)
     */
    protected function getObjPage()
    {
        return isset($GLOBALS['objPage']) ? $GLOBALS['objPage'] : null;
    }
}
