<?php

/**
 * @copyright: Copyright (c) 2018, Present Progressive GbR
 * @author: Benedict Zinke <bz@presentprogressive.de>
 * @license : MIT
 */

namespace PresProg\TemporalAttributesBundle\Attribute;

use Doctrine\DBAL\Connection;
use MetaModels\Attribute\AbstractSimpleAttributeTypeFactory;
use MetaModels\Helper\TableManipulator;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Attribute type factory for date attributes.
 */
class AttributeTimeTypeFactory extends AbstractSimpleAttributeTypeFactory
{
    /**
     * The event dispatcher.
     *
     * @var EventDispatcherInterface
     */
    private $dispatcher;

    /**
     * Create a new instance.
     *
     * @param Connection               $connection       Database connection.
     * @param TableManipulator         $tableManipulator The table manipulator.
     * @param EventDispatcherInterface $dispatcher       The event dispatcher.
     */
    public function __construct(
        Connection $connection,
        TableManipulator $tableManipulator,
        EventDispatcherInterface $dispatcher
    ) {
        parent::__construct($connection, $tableManipulator);

        $this->typeName   = 'time';
        $this->typeIcon   = 'bundles/metamodelsattributedate/date.png';
        $this->typeClass  = Time::class;
        $this->dispatcher = $dispatcher;
    }

    /**
     * {@inheritDoc}
     */
    public function createInstance($information, $metaModel)
    {
        return new Time($metaModel, $information, $this->connection, $this->tableManipulator, $this->dispatcher);
    }
}
