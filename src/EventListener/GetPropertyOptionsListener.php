<?php

/**
 * @copyright: Copyright (c) 2018, Present Progressive GbR
 * @author: Benedict Zinke <bz@presentprogressive.de>
 * @license : MIT
 */

namespace PresProg\TemporalAttributesBundle\EventListener;

use ContaoCommunityAlliance\DcGeneral\Contao\View\Contao2BackendView\Event\GetPropertyOptionsEvent;
use MetaModels\AttributeSelectBundle\Attribute\AbstractSelect;
use MetaModels\DcGeneral\Data\Model;

/**
 * The subscriber for the get filter options call.
 */
class GetPropertyOptionsListener
{
    /**
     * Retrieve the property options.
     *
     * @param GetPropertyOptionsEvent $event The event.
     *
     * @return void
     */
    public static function getPropertyOptions(GetPropertyOptionsEvent $event)
    {
        return [];
        if ($event->getOptions() !== null) {
            return;
        }

//        $model = $event->getModel();
//
//        if (!($model instanceof Model)) {
//            return;
//        }
//        $attribute = $model->getItem()->getAttribute($event->getPropertyName());
//
//        if (!($attribute instanceof AbstractSelect)) {
//            return;
//        }
//
//        try {
//            $options = $attribute->getFilterOptionsForDcGeneral();
//        } catch (\Exception $exception) {
//            $options = ['Error: ' . $exception->getMessage()];
//        }
//
//        $event->setOptions($options);
    }
}
