<?php

/**
 * @copyright: Copyright (c) 2018, Present Progressive GbR
 * @author: Benedict Zinke <bz@presentprogressive.de>
 * @license : MIT
 */

namespace PresProg\TemporalAttributesBundle\FilterSetting;

use MetaModels\Filter\FilterUrlBuilder;
use MetaModels\FilterTagsBundle\FilterSetting\TagsFilterSettingTypeFactory;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Attribute type factory for tags filter settings.
 */
class TagsWithTemporalsFilterSettingTypeFactory extends TagsFilterSettingTypeFactory
{
    /**
     * {@inheritDoc}
     */
    public function __construct(EventDispatcherInterface $dispatcher, FilterUrlBuilder $filterUrlBuilder)
    {
        parent::__construct($dispatcher, $filterUrlBuilder);
        foreach ([
            'date',
            'time',
        ] as $attribute) {
            $this->addKnownAttributeType($attribute);
        }
    }
}
