<?php

/**
 * @copyright: Copyright (c) 2018, Present Progressive GbR
 * @author: Benedict Zinke <bz@presentprogressive.de>
 * @license : MIT
 */

$GLOBALS['TL_DCA']['tl_metamodel_dcasetting']['metasubselectpalettes']['attr_id']['date'] = [
    'presentation' => [
        'tl_class',
    ],
    'functions'    => [
        'mandatory',
    ],
    'overview'     => [
        'filterable',
        'searchable',
    ],
];


$GLOBALS['TL_DCA']['tl_metamodel_dcasetting']['metasubselectpalettes']['attr_id']['time'] = [
    'presentation' => [
        'tl_class',
    ],
    'functions'    => [
        'mandatory',
    ],
    'overview'     => [
        'filterable',
        'searchable',
    ],
];