<?php

/**
 * @copyright: Copyright (c) 2018, Present Progressive GbR
 * @author: Benedict Zinke <bz@presentprogressive.de>
 * @license : MIT
 */

$GLOBALS['TL_DCA']['tl_metamodel_attribute']['metapalettes']['date extends _simpleattribute_'] = [];
$GLOBALS['TL_DCA']['tl_metamodel_attribute']['metapalettes']['time extends _simpleattribute_'] = [];
