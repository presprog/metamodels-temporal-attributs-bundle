<?php

/**
 * @copyright: Copyright (c) 2018, Present Progressive GbR
 * @author: Benedict Zinke <bz@presentprogressive.de>
 * @license : MIT
 */

$GLOBALS['TL_DCA']['tl_metamodel_rendersetting']['metapalettes']['date extends default'] = [];
$GLOBALS['TL_DCA']['tl_metamodel_rendersetting']['metapalettes']['time extends default'] = [];
